import isPage from "./isPage";
import IRuler from "./IRuler";

export interface IContainerProps {
	appName: string;
	components?: any[];
	pages?: IPageComponents[];
	services?: any[];
	config?: any;
	ruler?: IRuler;
}

export interface IPageComponents {
	pageRefs: string[];
	components: any[];
	services?: any[];
}

export interface IContainerContext {
	config: any;
	getService: <T>(serviceName: string) => T | false;
}

declare global {
	interface Window {
		m3Apps: Record<string, Container>;
	}

	interface Document {
		attachEvent:
			| ((event: string, listener: EventListener) => boolean | false)
			| undefined;
	}
}

function isPromise(promise: any) {
	return !!promise && typeof promise.then === "function";
}

export default class Container {
	private ruler: IRuler;
	private appName: string;
	private config: any;
	private componentsConfig: any;
	private components: any[];
	private pageComponents: IPageComponents[];
	private services: any[];
	private serviceMap: Record<string, any>;
	private instances: Record<string, object>;
	private ctx: IContainerContext;

	constructor({
		appName,
		components,
		pages,
		services,
		config,
		ruler,
	}: IContainerProps) {
		this.appName = appName;
		this.config = config;

		this.pageComponents = pages ? [...pages] : [];
		this.components = components ? [...components] : [];

		this.services = services ? [...services] : [];
		this.serviceMap = {};

		this.instances = {};
		this.componentsConfig = {};

		this.ruler = ruler ? ruler : new isPage();

		this.ctx = this.createContext.call(this);
	}

	private createContext(): IContainerContext {
		return {
			config: this.config,
			getService: this.getService.bind(this),
		};
	}

	private async initializeClass(compType: "comp" | "service", comp: any) {
		try {
			const type = typeof comp;
			if (type !== "function" && !isPromise(comp)) {
				console.warn("Not an Constructor or dynamic import", comp);
				return null;
			}
			let Comp = comp;
			if (isPromise(comp)) {
				Comp = (await comp).default;
			}

			if (compType === "service") this.serviceMap[Comp.name] = new Comp();

			if (compType === "comp") {
				if (this.componentsConfig[Comp.name]) {
					this.instances[Comp.name] = new Comp(
						this.ctx,
						this.componentsConfig[Comp.name]
					);
				} else {
					this.instances[Comp.name] = new Comp(this.ctx);
				}
			}

			return Comp.name;
		} catch (error) {
			console.warn(error);
			return null;
		}
	}

	private getService<T>(serviceName: string): T | false {
		if (this.serviceMap[serviceName]) return this.serviceMap[serviceName];
		return false;
	}

	private buildServices() {
		this.pageComponents.forEach((item) => {
			if (typeof item.services !== "undefined") {
				if (item.hasOwnProperty("pageRefs"))
					if (this.ruler.is(item.pageRefs)) {
						item.services.forEach((service) =>
							this.services.push(service)
						);
					}
			}
		});

		return this.services.map((s) => this.initializeClass("service", s));
	}

	private buildComponents() {
		return this.components.map((c) => this.initializeClass("comp", c));
	}

	private buildPageComponents() {
		return this.pageComponents.map((item) => {
			if (item.hasOwnProperty("pageRefs"))
				if (this.ruler.is(item.pageRefs)) {
					item.components.forEach((Comp) =>
						this.initializeClass("comp", Comp)
					);
				}
		});
	}

	public init() {
		this.buildServices.call(this);
		this.buildComponents.call(this);
		this.buildPageComponents.call(this);

		window["m3Apps"] = { [this.appName]: this };
	}

	public bind(compName: string, config: any) {
		this.componentsConfig[compName] = config;
	}

	public start() {
		if (
			document.attachEvent
				? document.readyState === "complete"
				: document.readyState !== "loading"
		) {
			this.init();
		} else {
			document.addEventListener("DOMContentLoaded", this.init.bind(this));
		}
	}
}
